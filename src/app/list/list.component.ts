import { Component, OnInit } from '@angular/core';
import {DataService} from '../data-service.service';



@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  constructor(
    public service: DataService
  ) {
  }

  ngOnInit(): void {
    if (this.service.mydata == null || this.service.mydata.length === 0) {
      this.service.fetch().subscribe(
        (data: any) => this.service.mydata = data,
        error => console.log(error),
        () => console.log('completed')

      );
    }
  }

  delete(data: any): void {

    for(let i = 0; i < this.service.mydata.length; i++){
      if(this.service.mydata[i].id == data.id){
        this.service.mydata.splice(i, 1);

      }
    }
  }
}
