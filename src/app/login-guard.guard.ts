import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, RouterModule, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {LoginService} from './login-service.service';
import {AppRoutingModule} from './app-routing.module';

@Injectable({
  providedIn: 'root'
})
export class LoginGuardGuard implements CanActivate {

constructor(public service: LoginService, public router: Router) {
}


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(this.service.isLoggedIn()){
      return true;
    }else {
      alert("Please login!");
      this.router.navigate(['login']);
      return false;
    }
  }

}
