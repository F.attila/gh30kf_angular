import { Component, OnInit } from '@angular/core';
import {DataService} from '../data-service.service';
import {DataModel} from '../data-model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  data = new DataModel();

  constructor(
    private service: DataService
  ) { }

  ngOnInit(): void {
  }

  save(): void {
    if (this.data.id >=
      0 && this.data.name.length > 0 && this.data.brewery_type.length > 0 && this.data.city.length > 0) {
      let found = false;
      for (let i = 0; i < this.service.mydata.length; i++) {
        if (this.service.mydata[i].id == this.data.id) {
          this.service.mydata[i] = this.data;
          found = true;
        }
      }
      if (!found) {
        this.service.mydata.push(this.data);
      }
      this.data = new DataModel();
    }else{
      alert("not valid data");
    }
  }


}
