import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DataModel} from './data-model';



@Injectable({
  providedIn: 'root'
})
export class DataService {

  public mydata: DataModel[] = [];

  constructor(
    private http: HttpClient
  ) { }

  public fetch(): Observable<any> {
    return this.http.get('https://api.openbrewerydb.org/breweries');
  }
}
