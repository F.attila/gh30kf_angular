import { Component, OnInit } from '@angular/core';
import {UserModel} from '../user-model';
import {DataModel} from '../data-model';
import {LoginGuardGuard} from '../login-guard.guard';
import {DataService} from '../data-service.service';
import {LoginService} from '../login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = new UserModel();

  constructor(public service: LoginService) { }

  ngOnInit(): void {
  }

  login(): void {
    if(this.user.username === 'aaaa' && this.user.password === 'bbbb'){
      this.service.userLogin();
      alert("Successful login!")
    }else{
      alert("Username and password doesn't match!");
    }
  }

}
