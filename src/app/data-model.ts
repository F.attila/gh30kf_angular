export class DataModel{
  id: number;
  name: string;
  brewery_type: string;
  city: string;
}
