import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public login = false;

  constructor() { }

  isLoggedIn() : boolean{

      return this.login;

  }

  userLogin() : any {
    this.login = true;
  }
}
